const express = require('express');
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require('../auth');

router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);	
	courseController.addCourse(userData, req.body).then(result => res.send(result));
})

router.get("/all", (req, res) => {
	courseController.getAllCourses().then(result => res.send(result));
})

router.get('/', (req, res) => {
	courseController.getAllActive().then(result => res.send(result));
})

router.get('/:courseId', (req, res) => {
	courseController.getCourse(req.params).then(result => res.send(result));
})

router.put('/:courseId', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	courseController.updateCourse(req.params, req.body, userData).then(result => res.send(result));
	
})

router.put('/:courseId/archive', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	courseController.archiveCourse(req.params, req.body, userData).then(result => res.send(result));
})


module.exports = router;
