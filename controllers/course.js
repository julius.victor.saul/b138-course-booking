const Course = require("../model/Course");

module.exports.addCourse = async (userData, reqBody) => {
	if(userData.isAdmin === false) {
		return "User is not authorized to create a new course";
	} else {
		let newCourse = new Course({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});

		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return "Course has been created";
			}
		})
	}
	
}

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

module.exports.updateCourse = async (reqParams, reqBody, userData) => {
	if(userData.isAdmin){
		let updatedCourse = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	} else {
		return "User has no access to update the course";
	}
}

module.exports.archiveCourse = async (reqParams, reqBody, userData) => {
	if(userData.isAdmin){
		return Course.findById(reqParams.courseId).then(result => {
			result.isActive = false;
			
			return result.save().then((course, error) => {
				if(error){
					return false;
				} else {
					return "Course has been archived";
				}
			})
		});

	} else {
		return "User has no access to archive the course";
	}
}
